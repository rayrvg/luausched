#include <luacode.h>
#include <taskscheduler/scheduler.hpp>
#include <taskscheduler/policies/queue.hpp>

#include <fstream>
#include <sstream>

#ifdef WIN32
#include <Windows.h>
#pragma comment(lib, "winmm.lib")
#endif

int main(int argc, char** argv) {
	if (argc < 2) {
		printf("missing input file\n");
		return 1;
	}
	
#ifdef WIN32
	timeBeginPeriod(1);
#endif

	std::ifstream file(argv[1]);
	std::stringstream ss;
	ss << file.rdbuf();
	file.close();

	auto src = ss.str();
	size_t bcsize;
	auto* bc = luau_compile(src.c_str(), src.size(), nullptr, &bcsize);

	scheduler_state state;
	state.policy = std::make_unique<queue_policy>();

	auto* task = state.new_task(state.main_state);
	lua_pop(state.main_state, 1);
	luau_load(task->thread, "main", bc, bcsize, 0);

	state.run_loop();
}