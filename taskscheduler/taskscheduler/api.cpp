#include <taskscheduler/api.hpp>
#include <taskscheduler/task.hpp>
#include <taskscheduler/scheduler.hpp>

#include <chrono>
#include <assert.h>

using namespace std::chrono;

void register_function(lua_State* L, lua_CFunction fn, const char* name, int nup = 0) {
	lua_pushcclosure(L, fn, name, nup);
	lua_setglobal(L, name);
}

void register_function(lua_State* L, lua_CFunction fn, const char* name, lua_Continuation cont, int nup = 0) {
	lua_pushcclosurek(L, fn, name, nup, cont);
	lua_setglobal(L, name);
}

struct wait_functor {
	high_resolution_clock::time_point wait_start;
	double wait_time_sec;

	wait_functor(double time) : wait_start(high_resolution_clock::now()), wait_time_sec(time) {}

	POLL_STATUS operator()() {
		auto dt = duration_cast<nanoseconds>(high_resolution_clock::now() - wait_start).count();
		return dt >= wait_time_sec * 1000000000 ? READY : WAITING ;
	}
};

int wait(lua_State* L) {
	luaL_checktype(L, 1, LUA_TNUMBER);

	double wait_time = lua_tonumber(L, 1);

	auto* task = get_thread_task(L);
	task->poll_fn = wait_functor(wait_time);

	return lua_yield(L, 0);
}

int wait_cont(lua_State* L, int status) {
	auto* task = get_thread_task(L);
	auto* functor = task->poll_fn->target<wait_functor>();
	assert(functor);
	lua_pushnumber(L, duration_cast<nanoseconds>(high_resolution_clock::now() - functor->wait_start).count() / 1000000000.0);
	return 1;
}

int spawn(lua_State* L) {
	luaL_checktype(L, 1, LUA_TFUNCTION);

	auto* ctask = get_thread_task(L);
	auto* state = ctask->state;
	auto* newtask = state->new_task(L);
	lua_insert(L, 1); // move thread to bottom of stack for return
	lua_xmove(L, newtask->thread, lua_gettop(L) - 1); // move func and args to new thread
	return 1;
}

void setup_api(lua_State* L) {
	register_function(L, wait, "wait", wait_cont);
	register_function(L, spawn, "spawn");
}