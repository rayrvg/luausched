#include <taskscheduler/task.hpp>
#include <taskscheduler/scheduler.hpp>
#include <taskscheduler/api.hpp>

#include <lstate.h>
#include <thread>
#include <chrono>
#include <stdio.h>

using namespace std::chrono;

scheduler_state::scheduler_state() {
	main_state = luaL_newstate();
	luaL_openlibs(main_state);
	setup_api(main_state);
}

scheduler_state::~scheduler_state() {
	lua_close(main_state);
}

lua_task* scheduler_state::new_task(lua_State* parent) {
	auto task_it = all_tasks.emplace(all_tasks.end(), std::make_unique<lua_task>(lua_newthread(parent), this));
	lua_task* task = task_it->get();
	task->parent_it = task_it;
	lua_setthreaddata((*task_it)->thread, task);
	policy->add_task(task);

	return task;
}

void scheduler_state::run_loop() {
	while (policy->get_num_tasks() > 0) {
		bool did_work = false;
		auto cycles = 0;
		while (cycles < all_tasks.size()) {
			cycles++;
			auto* task = policy->next_task();
			if (!task)
				break;
			if (task->poll() == WAITING) {
				policy->add_task(task);
				continue;
			}

			did_work = true;

			int result;
			if (lua_status(task->thread) == 0 && task->thread->ci == task->thread->base_ci)
				// initial execution, resume with args
				result = lua_resume(task->thread, main_state, lua_gettop(task->thread) - 1);
			else
				result = lua_resume(task->thread, main_state, 0);

			if (result != LUA_YIELD) {
				task->end();
			}
			else {
				policy->add_task(task);
			}
		}

		if (!did_work) {
			std::this_thread::sleep_for(milliseconds(1));
		}
	}
}