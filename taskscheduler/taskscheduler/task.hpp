#pragma once

#include <lualib.h>

#include <list>
#include <memory>
#include <functional>
#include <optional>

enum POLL_STATUS {
	WAITING,
	READY
};

typedef std::function<POLL_STATUS()> poll_t;

struct scheduler_state;
struct lua_task {
	int id;
	int priority = 0;
	std::optional<poll_t> poll_fn = std::nullopt;
	lua_State* thread;
	scheduler_state* state;
	std::list<std::unique_ptr<lua_task>>::iterator parent_it;

	lua_task(lua_State* thread, scheduler_state* state) : thread(thread), state(state) {
		static int _id = 0;
		id = _id++;
	}

	bool operator<(const lua_task& other) {
		return priority < other.priority;
	}

	POLL_STATUS poll();
	void end();
};


// struct task_wrapper {
// 	lua_task* task;
// 	task_holder& parent;
// 	task_holder::iterator parent_it;

// 	task_wrapper(lua_task* task, task_holder& parent, task_holder::iterator parent_it) : task(task), parent(parent), parent_it(parent_it) {}
// 	void end();
// };

lua_task* get_thread_task(lua_State* thread);