#include <taskscheduler/scheduler.hpp>
#include <taskscheduler/task.hpp>

POLL_STATUS lua_task::poll() {
	if (poll_fn) {
		return poll_fn.value()();
	}
	return READY;
}

void lua_task::end() {
	state->all_tasks.erase(parent_it);
}

lua_task* get_thread_task(lua_State* thread) {
	return static_cast<lua_task*>(lua_getthreaddata(thread));
}