#pragma once

#include <taskscheduler/task.hpp>

struct scheduler_policy {
	virtual void add_task(lua_task* task) = 0;
	// virtual lua_task* current_task() = 0;
	virtual lua_task* next_task() = 0;
	virtual size_t get_num_tasks() = 0;
	virtual ~scheduler_policy() = default;
};