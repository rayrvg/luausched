#pragma once

#include <taskscheduler/policies/basepolicy.hpp>

#include <queue>

struct queue_policy : scheduler_policy {
	std::queue<lua_task*> task_queue;

	queue_policy() {}

	void add_task(lua_task* task) override {
		task_queue.push(task);
	}

	lua_task* next_task() override {
		if (task_queue.empty())
			return nullptr;

		auto* ret = task_queue.front();
		task_queue.pop();
		return ret;
	}

	size_t get_num_tasks() override {
		return task_queue.size();
	}
};