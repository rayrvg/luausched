#pragma once

#include <taskscheduler/task.hpp>
#include <taskscheduler/policies/basepolicy.hpp>
#include <lualib.h>

#include <list>
#include <memory>

struct scheduler_state {
	lua_State* main_state;
	std::list<std::unique_ptr<lua_task>> all_tasks;
	std::unique_ptr<scheduler_policy> policy;

	scheduler_state();
	~scheduler_state();

	lua_task* new_task(lua_State* parent);
	void run_loop();

private:
	void add_task(lua_task* task);
};